<?php
/**
 * Created by PhpStorm
 * author  :gogochen
 * date    :2019/12/12
 * time    :11:13
 */

namespace App\Model;

/**
 * description  玩家
 * Class Player
 * @package App\Model
 */
class Player
{
    //定义游戏上下左右动作
    CONST UP = "up";
    CONST DOWN = "down";
    CONST LEFT = "left";
    CONST RIGHT = "right";

    CONST DIRECTION = [self::UP, self::DOWN, self::LEFT, self::RIGHT];

    //定义玩家类型，1为寻找者，2为躲藏者
    CONST PLAYER_TYPE_SEEK = 1;
    CONST PLAYER_TYPE_HIDE = 2;

    //定义玩家唯一id
    private $id;

    //定义玩家坐标
    private $x;
    private $y;

    //假设玩家所属类型
    private $type = self::PLAYER_TYPE_SEEK;

    public function __construct($id, $x, $y)
    {
        $this->id = $id;
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * description  设置玩家类型
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * description  获取玩家id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function up()
    {
        $this->x--;
    }

    public function down()
    {
        $this->x++;
    }


    public function left()
    {
        $this->y--;
    }


    public function right()
    {
        $this->y++;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getX()
    {
        return $this->x;
    }


    public function getY()
    {
        return $this->y;
    }

}
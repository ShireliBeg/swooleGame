<?php
/**
 * Created by PhpStorm
 * author  :gogochen
 * date    :2019/12/13
 * time    :22:41
 */

namespace App\Lib;


class Redis
{
    protected static $instance;

    protected static $config = [
        'host' => '127.0.0.1',
        'port' => 6379
    ];


    /**
     * description  获取redis实例
     * @return \Redis
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            $instance = new \Redis();
            $instance->connect(
                self::$config['host'],
                self::$config['port']
            );
            self::$instance = $instance;
        }
        return self::$instance;
    }
}
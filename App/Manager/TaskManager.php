<?php
/**
 * Created by PhpStorm
 * author  :gogochen
 * date    :2019/12/17
 * time    :18:31
 */

namespace App\Manager;


class TaskManager
{
    //发起寻找玩家task任务
    CONST TASK_CODE_FIND_PLAYER = 1;


    public static function findPlayer()
    {
        $playerListLen = DataCenter::getPlayerWaitListLen();
        if ($playerListLen >= 2) {
            $redPlayer = DataCenter::popPlayerFromWaitList();
            $bluePlayer = DataCenter::popPlayerFromWaitList();
            return [
                'red_player'=>$redPlayer,
                'blue_player'=>$bluePlayer
            ];
        }

        return false;
    }
}